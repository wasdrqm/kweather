package com.wasdrqm.kweather.data.network

import com.github.ajalt.timberkt.e
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.Type

class RxErrorHandlingCallAdapterFactory : CallAdapter.Factory() {

    companion object {
        fun create() = RxErrorHandlingCallAdapterFactory()
    }

    private val _original by lazy {
        RxJava2CallAdapterFactory.create()
    }

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *> {
        val wrapped = _original.get(returnType, annotations, retrofit) as CallAdapter<*, *>
        return RxCallAdapterWrapper(retrofit, wrapped)
    }

    private class RxCallAdapterWrapper<R>(
        val retrofit: Retrofit, val wrappedCallAdapter: CallAdapter<R, *>
    ) : CallAdapter<R, Any> {

        override fun responseType(): Type = wrappedCallAdapter.responseType()

        override fun adapt(call: Call<R>): Any = when (val result = wrappedCallAdapter.adapt(call)) {
            is Completable -> result.onErrorResumeNext { throwable -> Completable.error(throwable) }
            is Single<*> -> result.onErrorResumeNext { throwable -> Single.error(handleException(throwable)) }
            is Flowable<*> -> result.onErrorResumeNext(Function { throwable -> Flowable.error(handleException(throwable)) })
            is Observable<*> -> result.onErrorResumeNext(Function { throwable -> Observable.error(handleException(throwable)) })
            else -> result
        }

        private fun handleException(throwable: Throwable?): Throwable? {
            if (throwable is HttpException) {
                val converter = retrofit.responseBodyConverter<ServerError>(
                    ServerError::class.java, arrayOfNulls(0)
                )
                val response = throwable.response()
                return try {
                    var error: ServerError? = null
                    val body = response?.errorBody()
                    if (body != null) {  // contentLength may be -1 if is unknown
                        error = converter.convert(body)
                    }
                    ServerException(throwable.code(), throwable.message(), error)
                } catch (exception: IOException) {
                    e(exception) { "#api error in handleException method" }
                    exception
                }
            }
            return throwable
        }

    }

}