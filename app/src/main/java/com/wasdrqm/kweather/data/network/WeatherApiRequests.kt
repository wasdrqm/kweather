package com.wasdrqm.kweather.data.network

import com.wasdrqm.kweather.data.models.responses.CurrentWeatherResponse
import com.wasdrqm.kweather.data.models.responses.DaysWeatherResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface WeatherApiRequests {

    @GET("/data/2.5/weather")
    fun fetchWeather(
            @Query("q") city: String?,
            @Query("appid") appId: String,
            @Query("units") units: String,
            @Query("lang") lang: String
    ): Single<CurrentWeatherResponse>

    @GET("/data/2.5/forecast/daily")
    fun fetchWeatherDays(
            @Query("q") city: String?,
            @Query("mode") mode: String,
            @Query("units") units: String,
            @Query("cnt") counter: Int?,
            @Query("appid") appId: String,
            @Query("lang") lang: String
    ): Single<DaysWeatherResponse>

}