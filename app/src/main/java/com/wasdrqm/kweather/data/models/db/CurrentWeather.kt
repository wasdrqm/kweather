package com.wasdrqm.kweather.data.models.db

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class CurrentWeather : RealmObject() {
    @PrimaryKey
    open var id: String? = null
    open var datetime: Date? = null
    open var city: String? = null
    open var lang: String? = null
    open var description: String? = null
    open var temperature: Double? = null
    open var minTemperature: Double? = null
    open var maxTemperature: Double? = null
    open var humidity: Int? = null
    open var pressure: Double? = null
    open var sunset: Date? = null
    open var sunrise: Date? = null
}

open class DayWeather : RealmObject() {
    @PrimaryKey
    open var id: String? = null
    open var datetime: Date? = null
    open var city: String? = null
    open var lang: String? = null
    open var icon: String? = null
    open var description: String? = null
    open var morningTemperature: Double? = null
    open var dayTemperature: Double? = null
    open var eveningTemperature: Double? = null
    open var nightTemperature: Double? = null
}