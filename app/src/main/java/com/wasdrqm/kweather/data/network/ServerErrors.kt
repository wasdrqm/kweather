package com.wasdrqm.kweather.data.network

/**
 * Data class represents entity of a server error.
 */
data class ServerError(val message: String?, val errors: HashMap<String, List<String>>?) {
    override fun toString() = if (errors != null) "$message: $errors" else message ?: "no error message"
}

/**
 * Exception for an unexpected, non-2xx HTTP response. Same as HttpException but contains ServerError.
 */
data class ServerException(
    val code: Int, override val message: String?, val serverError: ServerError?
) : RuntimeException() {
    override fun toString() = serverError?.toString() ?: "$code $message"
}
