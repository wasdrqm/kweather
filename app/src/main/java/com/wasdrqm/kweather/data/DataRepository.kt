package com.wasdrqm.kweather.data

import com.wasdrqm.kweather.BuildConfig
import com.wasdrqm.kweather.data.models.db.CurrentWeather
import com.wasdrqm.kweather.data.models.db.DayWeather
import com.wasdrqm.kweather.data.network.WeatherApiRequests
import com.wasdrqm.kweather.extensions.asFirstObjectCopyObservable
import com.wasdrqm.kweather.extensions.asListSortObservable
import com.wasdrqm.kweather.extensions.toCurrentWeather
import com.wasdrqm.kweather.extensions.toDayWeathers
import io.realm.Realm
import io.realm.Sort
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepository @Inject constructor() {

    companion object {
        private const val DATA_FORMAT = "json"
        private const val UNITS_FORMAT = "metric"
        private const val COUNT: Int = 5
    }

    @Inject lateinit var api: WeatherApiRequests

    // work with api

    fun fetchCurrentWeather(city: String?, lang: String = Locale.getDefault().language) = api
        .fetchWeather(city, BuildConfig.APP_ID, UNITS_FORMAT, lang)
        .map { response -> response.toCurrentWeather(city, lang) }

    fun fetchWeatherDays(city: String?, lang: String = Locale.getDefault().language) = api
        .fetchWeatherDays(city, DATA_FORMAT, UNITS_FORMAT, COUNT, BuildConfig.APP_ID, lang)
        .map { response -> response.toDayWeathers(city, lang) }

    // save data in realm

    fun saveCurrentWeather(realm: Realm, weather: CurrentWeather) {
        realm.where(CurrentWeather::class.java)
            .equalTo("city", weather.city)
            .equalTo("lang", weather.lang)
            .findAll()
            .deleteAllFromRealm()
        realm.copyToRealmOrUpdate(weather)
    }

    fun saveWeatherDays(realm: Realm, list: List<DayWeather>) {
        val city = list.first().city
        val lang = list.first().lang
        realm.where(DayWeather::class.java)
            .equalTo("city", city)
            .equalTo("lang", lang)
            .findAll()
            .deleteAllFromRealm()
        realm.copyToRealmOrUpdate(list)
    }

    // subscribe on realm

    fun getCurrentWeather(
        instance: Realm, city: String?, lang: String = Locale.getDefault().language
    ) = instance
        .where(CurrentWeather::class.java)
        .equalTo("lang", lang)
        .equalTo("city", city)
        .findAllAsync()
        .sort("datetime", Sort.DESCENDING)
        .asFirstObjectCopyObservable(instance)

    fun getWeatherDays(
        instance: Realm, city: String?, lang: String = Locale.getDefault().language
    ) = instance
        .where(DayWeather::class.java)
        .equalTo("lang", lang)
        .equalTo("city", city)
        .findAllAsync()
        .asListSortObservable(arrayOf("datetime"), arrayOf(Sort.DESCENDING))

}