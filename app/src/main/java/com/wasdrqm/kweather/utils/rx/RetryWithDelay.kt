package com.wasdrqm.kweather.utils.rx

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.functions.Function
import org.reactivestreams.Publisher
import java.util.concurrent.TimeUnit


class RetryWithDelayObservable(
    private val retryDelayMillis: Long, private var maxRetries: Int
) : Function<Observable<Throwable>, ObservableSource<Any>> {

    private var retryCount: Int = 0

    override fun apply(attempts: Observable<Throwable>): ObservableSource<Any> = attempts.flatMap { throwable ->
        if (maxRetries == -1 || ++retryCount < maxRetries) {
            Observable.timer(retryDelayMillis, TimeUnit.MILLISECONDS)
        } else {
            Observable.error(throwable)
        }
    }

}

class RetryWithDelaySingle(
    private val retryDelayMillis: Long, private var maxRetries: Int
) : Function<Flowable<Throwable>, Publisher<Any>> {

    private var retryCount: Int = 0

    override fun apply(attempts: Flowable<Throwable>): Publisher<Any> = attempts.flatMap { throwable ->
        if (maxRetries == -1 || ++retryCount < maxRetries) {
            Flowable.timer(retryDelayMillis, TimeUnit.MILLISECONDS)
        } else {
            Flowable.error(throwable)
        }
    }

}