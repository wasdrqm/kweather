package com.wasdrqm.kweather.utils

import android.content.res.Resources
import android.util.TypedValue

object ViewUtils {

    private fun convertValueToDp(resources: Resources, value: Int) : Float {
        val displayMetrics = resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value.toFloat(), displayMetrics)
    }

    fun roundConvertValueToDp(resources: Resources, value: Int) = convertValueToDp(resources, value).toInt()

}