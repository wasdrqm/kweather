package com.wasdrqm.kweather.utils.recyclerview

import android.content.res.Resources
import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.wasdrqm.kweather.utils.ViewUtils

class MarginItemDecorator : RecyclerView.ItemDecoration {

    private var topOffset: Int = 0
    private var bottomOffset: Int = 0
    private var leftOffset: Int = 0
    private var rightOffset: Int = 0
    private val resources: Resources

    constructor(offSet: Int, resources: Resources) {
        bottomOffset = offSet
        leftOffset = offSet
        rightOffset = offSet
        topOffset = offSet
        this.resources = resources
    }

    constructor(topOffset: Int, bottomOffset: Int, leftOffset: Int, rightOffset: Int, resources: Resources) {
        this.bottomOffset = bottomOffset
        this.leftOffset = leftOffset
        this.rightOffset = rightOffset
        this.topOffset = topOffset
        this.resources = resources
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.left = ViewUtils.roundConvertValueToDp(resources, leftOffset)
        outRect.right = ViewUtils.roundConvertValueToDp(resources, rightOffset)
        outRect.top = ViewUtils.roundConvertValueToDp(resources, topOffset)
        outRect.bottom = ViewUtils.roundConvertValueToDp(resources, bottomOffset)
    }

}