package com.wasdrqm.kweather.utils

import com.wasdrqm.kweather.core.WeatherApplication
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {

    const val DATE_FORMAT_DATE = "yyyy.MM.dd"

    private const val DATE_FORMAT_TIME = "HH:mm"
    private const val MOSCOW = "Europe/Moscow"
    private const val NOVOSIBIRSK = "Asia/Novosibirsk"
    private const val TASHKENT = "Asia/Tashkent"

    fun getFormatDateTime(date: Date, city: String?, format: String? = DATE_FORMAT_TIME): String {
        val timezone = when (city) {
            WeatherApplication.MOSCOW -> MOSCOW
            WeatherApplication.NOVOSIBIRSK -> NOVOSIBIRSK
            WeatherApplication.TASHKENT -> TASHKENT
            else -> TimeZone.getDefault().displayName
        }
        val simpleDateFormat = SimpleDateFormat(format, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone(timezone)
        return simpleDateFormat.format(date)
    }

}