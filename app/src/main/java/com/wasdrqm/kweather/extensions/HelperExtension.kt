package com.wasdrqm.kweather.extensions

import android.content.Context
import com.wasdrqm.kweather.R
import com.wasdrqm.kweather.core.WeatherApplication
import com.wasdrqm.kweather.data.models.db.CurrentWeather
import com.wasdrqm.kweather.data.models.db.DayWeather
import com.wasdrqm.kweather.data.models.responses.CurrentWeatherResponse
import com.wasdrqm.kweather.data.models.responses.DaysWeatherResponse
import com.wasdrqm.kweather.data.models.responses.Weathers
import java.util.*

fun Long.toDate() = if (this > 0) Date(this * 1000L) else null

fun String.toCityNameResString(context: Context) = when(this) {
    WeatherApplication.MOSCOW -> context.getString(R.string.city_moscow)
    WeatherApplication.NOVOSIBIRSK -> context.getString(R.string.city_novosibirsk)
    WeatherApplication.TASHKENT -> context.getString(R.string.city_tashkent)
    else -> null
}

fun CurrentWeatherResponse.toCurrentWeather(city: String?, lang: String?) = CurrentWeather().apply {
    this.id = UUID.randomUUID().toString()
    this.city = city
    this.lang = lang
    this.description = this@toCurrentWeather.weather?.firstOrNull()?.description
    this.datetime = this@toCurrentWeather.datetime.toDate()
    this.temperature = this@toCurrentWeather.main?.temperature
    this.minTemperature = this@toCurrentWeather.main?.temperatureMin
    this.maxTemperature = this@toCurrentWeather.main?.temperatureMax
    this.humidity = this@toCurrentWeather.main?.humidity
    this.pressure = this@toCurrentWeather.main?.pressure
    this.sunrise = this@toCurrentWeather.weatherSystem?.sunrise?.toDate()
    this.sunset = this@toCurrentWeather.weatherSystem?.sunset?.toDate()
}

fun DaysWeatherResponse.toDayWeathers(city: String?, lang: String?) = mutableListOf<DayWeather>().apply {
    this@toDayWeathers.weathers?.forEach { weathers ->
        add(weathers.toDayWeather(city, lang))
    }
}

fun Weathers.toDayWeather(city: String?, lang: String?) = DayWeather().apply {
    this.id = UUID.randomUUID().toString()
    this.city = city
    this.lang = lang
    this.datetime = this@toDayWeather.datetime.toDate()
    this.morningTemperature = this@toDayWeather.temperature?.morning
    this.dayTemperature = this@toDayWeather.temperature?.day
    this.eveningTemperature = this@toDayWeather.temperature?.eve
    this.nightTemperature = this@toDayWeather.temperature?.night
    this@toDayWeather.weathers?.firstOrNull()?.let { weather ->
        this.icon = weather.icon
        this.description = weather.description
    }
}