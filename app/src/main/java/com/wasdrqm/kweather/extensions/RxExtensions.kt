package com.wasdrqm.kweather.extensions

import com.github.ajalt.timberkt.e
import com.wasdrqm.kweather.data.network.ServerException
import com.wasdrqm.kweather.utils.rx.RetryWithDelayObservable
import com.wasdrqm.kweather.utils.rx.RetryWithDelaySingle
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.concurrent.TimeUnit

// threads

fun <T> Single<T>.applyNetworkSchedulers(): Single<T> {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.applyNetworkSchedulers(): Observable<T> {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Flowable<T>.applyNetworkSchedulers(): Flowable<T> {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun Completable.applyNetworkSchedulers(): Completable {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

// retry with delay

fun <T> Single<T>.retryWithDelay(retryDelayMillis: Long, maxRetries: Int = -1): Single<T> {
    return this.retryWhen(RetryWithDelaySingle(retryDelayMillis, maxRetries))
}

fun Completable.retryWithDelay(retryDelayMillis: Long, maxRetries: Int = -1): Completable {
    return this.retryWhen(RetryWithDelaySingle(retryDelayMillis, maxRetries))
}

fun <T> Observable<T>.retryWithDelay(retryDelayMillis: Long, maxRetries: Int = -1): Observable<T> {
    return this.retryWhen(RetryWithDelayObservable(retryDelayMillis, maxRetries))
}

// repeat with delay

fun <T> Single<T>.repeatWithDelay(delayMills: Long): Flowable<T> {
    return this.repeatWhen { completed -> completed.delay(delayMills, TimeUnit.MILLISECONDS) }
}

fun <T> Observable<T>.repeatWithDelay(delayMills: Long): Observable<T> {
    return this.repeatWhen { completed -> completed.delay(delayMills, TimeUnit.MILLISECONDS) }
}

// error handling

fun <T> Single<T>.handleError(serverAndNetwork: (String) -> Unit = {}): Single<T> {
    return this.onErrorResumeNext { ex ->
        when (ex) {
            is ServerException -> {
                e(ex) { "Server error" }
                serverAndNetwork(ex.toString())
            }
            is IOException -> {
                e(ex) { "Network error" }
                serverAndNetwork("Network error")
            }
            else -> {
                e(ex)
                serverAndNetwork("Error")
            }
        }
        Single.never()
    }
}

fun <T> Observable<T>.handleError(serverAndNetwork: (String) -> Unit = {}): Observable<T> {
    return this.onErrorResumeNext(io.reactivex.functions.Function { ex ->
        when (ex) {
            is ServerException -> {
                e(ex) { "Server error" }
                serverAndNetwork(ex.toString())
            }
            is IOException -> {
                e(ex) { "Network error" }
                serverAndNetwork("Network error")
            }
            else -> {
                e(ex)
                serverAndNetwork("Error")
            }
        }
        Observable.empty()
    })
}

fun Completable.handleError(serverAndNetwork: (String) -> Unit = {}): Completable {
    return this.onErrorResumeNext { ex ->
        when (ex) {
            is ServerException -> {
                e(ex) { "Server error" }
                serverAndNetwork(ex.toString())
            }
            is IOException -> {
                e(ex) { "Network error" }
                serverAndNetwork("Network error")
            }
            else -> {
                e(ex)
                serverAndNetwork("Error")
            }
        }
        Completable.complete()
    }
}

fun <T> Flowable<T>.handleError(serverAndNetwork: (String) -> Unit = {}): Flowable<T> {
    return this.onErrorResumeNext(io.reactivex.functions.Function { ex ->
        when (ex) {
            is ServerException -> {
                e(ex) { "Server error" }
                serverAndNetwork(ex.toString())
            }
            is IOException -> {
                e(ex) { "Network error" }
                serverAndNetwork("Network error")
            }
            else -> {
                e(ex)
                serverAndNetwork("Error")
            }
        }
        Flowable.empty()
    })
}