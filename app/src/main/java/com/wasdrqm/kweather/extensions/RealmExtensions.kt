package com.wasdrqm.kweather.extensions

import io.reactivex.Observable
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.Sort

// inline for transactions

inline fun <T: Any> Single<T>.realmTransaction(
    realm: Realm, crossinline transaction: (Realm, T) -> Unit
): Single<T> = this.flatMap { result ->
    Single.create { emitter ->
        val task = realm.executeTransactionAsync(
            { transaction(it, result) },
            { if (!emitter.isDisposed) emitter.onSuccess(result) },
            { emitter.onError(it) })
        emitter.setCancellable { if (!task.isCancelled) task.cancel() }
    }
}

inline fun <T: Any> Observable<T>.realmTransaction(
    realm: Realm, crossinline transaction: (Realm, T) -> Unit
): Observable<T> = this.flatMap { result ->
    Observable.create { emitter ->
        val task = realm.executeTransactionAsync(
            { transaction(it, result) },
            {
                if (!emitter.isDisposed) {
                    emitter.onNext(result)
                    emitter.onComplete()
                }
            },
            { emitter.onError(it) })
        emitter.setCancellable { if (!task.isCancelled) task.cancel() }
    }
}

// help functions

fun <E : RealmObject> RealmResults<E>.asListObservable(): Observable<RealmResults<E>> = this
    .asChangesetObservable()
    .filter { it.collection.isNotEmpty() }
    .map { it.collection }
    .filter { it.isLoaded && it.isValid }

fun <E : RealmObject> RealmResults<E>.asListEmptyObservable(): Observable<RealmResults<E>> = this
    .asChangesetObservable()
    .map { it.collection }
    .filter { it.isLoaded && it.isValid }

fun <E : RealmObject> RealmResults<E>.asListSortObservable(
    arrayOfColumns: Array<String>, arrayOfSort: Array<Sort>
): Observable<RealmResults<E>> = this
    .asChangesetObservable()
    .filter { it.collection.isNotEmpty() }
    .map { it.collection.sort(arrayOfColumns, arrayOfSort) }
    .filter { it.isLoaded && it.isValid }

fun <E : RealmObject> RealmResults<E>.asListEmptySortObservable(
    arrayOfColumns: Array<String>, arrayOfSort: Array<Sort>
): Observable<RealmResults<E>> = this
    .asChangesetObservable()
    .map { it.collection.sort(arrayOfColumns, arrayOfSort) }
    .filter { it.isLoaded && it.isValid }

fun <E : RealmObject> RealmResults<E>.asFirstObjectObservable(): Observable<E> = this
    .asListObservable()
    .filter { it.isNotEmpty() }
    .map { it.first() }

fun <E : RealmObject> RealmResults<E>.asFirstObjectCopyObservable(realm: Realm): Observable<E> = this
    .asListObservable()
    .filter { it.isNotEmpty() }
    .map { it.first()?.let(realm::copyFromRealm) ?: throw NullPointerException() }

fun <E : RealmObject> RealmResults<E>.asListCopyObservable(realm: Realm): Observable<MutableList<E>> = this
    .asChangesetObservable()
    .filter { it.collection.isNotEmpty() }
    .map { it.collection }
    .filter { it.isLoaded && it.isValid }
    .map { realm.copyFromRealm(it) }

fun <E : RealmObject> RealmResults<E>.asListCopySortObservable(
    realm: Realm, arrayOfColumns: Array<String>, arrayOfSort: Array<Sort>
): Observable<MutableList<E>> = this
    .asChangesetObservable()
    .filter { it.collection.isNotEmpty() }
    .map { it.collection }
    .filter { it.isLoaded && it.isValid }
    .map { realm.copyFromRealm(it.sort(arrayOfColumns, arrayOfSort)) }

fun <E: RealmObject> RealmResults<E>.asFilterSortObservable(
    arrayOfColumns: Array<String>, arrayOfSort: Array<Sort>
): Observable<RealmResults<E>> = this
    .asChangesetObservable()
    .map { it.collection }
    .filter { it.isLoaded && it.isValid }
    .map { it.sort(arrayOfColumns, arrayOfSort) }

fun <E : RealmObject> RealmResults<E>.asFirstObjectCopySingle(realm: Realm): Single<E> = this
    .asListObservable()
    .filter { it.isNotEmpty() }
    .map { it.first()?.let(realm::copyFromRealm) ?: throw NullPointerException() }
    .firstOrError()

fun <E : RealmObject> RealmResults<E>.asListCopySortSingle(
    realm: Realm, arrayOfColumns: Array<String>, arrayOfSort: Array<Sort>
): Single<MutableList<E>> = this
    .asChangesetObservable()
    .filter { it.collection.isNotEmpty() }
    .map { it.collection }
    .filter { it.isLoaded && it.isValid }
    .map { realm.copyFromRealm(it.sort(arrayOfColumns, arrayOfSort)) }
    .firstOrError()

fun Realm.executeTransaction(work: (Realm) -> Unit, onSuccess: () -> Unit) {
    this.executeTransactionAsync(Realm.Transaction { work(it) }, Realm.Transaction.OnSuccess { onSuccess() })
}