package com.wasdrqm.kweather.view.base.activities

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.evernote.android.state.StateSaver
import com.wasdrqm.kweather.core.WeatherApplication
import com.wasdrqm.kweather.core.di.components.ActivityComponent
import com.wasdrqm.kweather.core.di.components.DaggerActivityComponent

abstract class BaseActivity<B: ViewBinding> : AppCompatActivity() {

    private var _binding: B? = null

    @Suppress("UNCHECKED_CAST")
    protected open val binding get() = _binding as B
    abstract val bindingInflater: (LayoutInflater) -> B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = bindingInflater.invoke(layoutInflater)
        setContentView(requireNotNull(_binding).root)

        if (savedInstanceState == null) initOrRestoreArgs(intent.extras)
        else StateSaver.restoreInstanceState(this, savedInstanceState)

        initComponent(DaggerActivityComponent.builder().appComponent(WeatherApplication.appComponent).build())
        initCore()
        initViews()
    }

    // lifecycle

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this, outState)
    }

    // public methods

    /**
     * init di component
     */
    protected open fun initComponent(component: ActivityComponent) {}

    /**
     * init presenters or dagger component
     */
    open fun initCore() {}

    /**
     * init intent extra values
     */
    open fun initOrRestoreArgs(bundle: Bundle?) {}

    /**
     * init layout views and actions
     */
    open fun initViews() {}

}
