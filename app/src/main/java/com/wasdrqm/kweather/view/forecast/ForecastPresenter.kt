package com.wasdrqm.kweather.view.forecast

import com.github.ajalt.timberkt.d
import com.github.ajalt.timberkt.e
import com.wasdrqm.kweather.core.di.components.PresenterComponent
import com.wasdrqm.kweather.data.DataRepository
import com.wasdrqm.kweather.data.models.db.DayWeather
import com.wasdrqm.kweather.extensions.applyNetworkSchedulers
import com.wasdrqm.kweather.extensions.handleError
import com.wasdrqm.kweather.extensions.realmTransaction
import com.wasdrqm.kweather.extensions.retryWithDelay
import com.wasdrqm.kweather.view.base.IMvpView
import com.wasdrqm.kweather.view.base.presenter.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import io.realm.RealmResults
import javax.inject.Inject

class ForecastPresenter(private val city: String?) : BasePresenter<IForecastView>() {

    @Inject lateinit var dataRepository: DataRepository

    val fetchForecastAction: PublishSubject<Unit> = PublishSubject.create()

    override fun initComponent(component: PresenterComponent) {
        component.inject(this)
    }

    override fun onCreateAction(subscriptions: CompositeDisposable) {
        subscriptions.add(fetchForecastAction
            .flatMapSingle { dataRepository
                .fetchWeatherDays(city)
                .handleError { error -> e { "#forecast-presenter fetch forecast weather error, message: $error" } }
                .retryWithDelay(3000L, 3)
                .applyNetworkSchedulers()
            }
            .realmTransaction(realm, dataRepository::saveWeatherDays)
            .subscribe(
                { d { "#forecast-presenter fetch forecast weather success" } },
                { throwable -> e(throwable) { "#forecast-presenter fetch forecast weather error" } }
            )
        )
        fetchForecastAction.onNext(Unit)
    }

    override fun onStartAction(subscriptions: CompositeDisposable) {
        subscriptions.add(dataRepository
            .getWeatherDays(realm, city)
            .doOnSubscribe { view.showLoading(true) }
            .doOnNext { view.showLoading(false) }
            .subscribe(view::showWeathers) { throwable ->
                e(throwable) { "#forecast-presenter fetch forecast weather error" }
            }
        )
    }

}

interface IForecastView : IMvpView {
    fun showLoading(show: Boolean)
    fun showWeathers(weathers: RealmResults<DayWeather>)
}