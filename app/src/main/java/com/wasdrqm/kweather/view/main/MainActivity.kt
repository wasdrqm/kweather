package com.wasdrqm.kweather.view.main

import android.view.LayoutInflater
import android.view.View
import com.wasdrqm.kweather.R
import com.wasdrqm.kweather.core.WeatherApplication
import com.wasdrqm.kweather.databinding.ActivityMainBinding
import com.wasdrqm.kweather.view.base.activities.BaseActivity
import com.wasdrqm.kweather.view.weather.CurrentWeatherActivity

class MainActivity : BaseActivity<ActivityMainBinding>() {

    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding
        = ActivityMainBinding::inflate

    fun buttonClick(view: View) {
        when(view.id) {
            R.id.buttonMoscow -> WeatherApplication.MOSCOW
            R.id.buttonNovosibirsk -> WeatherApplication.NOVOSIBIRSK
            R.id.buttonTashkent -> WeatherApplication.TASHKENT
            else -> null
        }?.let { city ->
            CurrentWeatherActivity.openScreen(this, city)
        }
    }

}
