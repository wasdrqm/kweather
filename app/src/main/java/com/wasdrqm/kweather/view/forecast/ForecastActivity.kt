package com.wasdrqm.kweather.view.forecast

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.evernote.android.state.State
import com.wasdrqm.kweather.core.di.components.ActivityComponent
import com.wasdrqm.kweather.data.models.db.DayWeather
import com.wasdrqm.kweather.databinding.ActivityForecastBinding
import com.wasdrqm.kweather.extensions.toCityNameResString
import com.wasdrqm.kweather.utils.recyclerview.MarginItemDecorator
import com.wasdrqm.kweather.view.base.activities.BaseMvpActivity
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_forecast.*

class ForecastActivity
    : BaseMvpActivity<ActivityForecastBinding, IForecastView, ForecastPresenter>(), IForecastView {

    companion object {

        private const val EXTRA_CITY = "EXTRA::city"

        fun openScreen(context: Context, cityName: String?) {
            context.startActivity(
                Intent(context, ForecastActivity::class.java).putExtra(EXTRA_CITY, cityName)
            )
        }

    }

    @State var cityName: String? = null

    private lateinit var forecastAdapter: ForecastAdapter

    // abstract

    override val view = this
    override val bindingInflater: (LayoutInflater) -> ActivityForecastBinding
        = ActivityForecastBinding::inflate

    override fun getPresenterCode() = (ForecastActivity::class.java.name + cityName).hashCode()
    override fun createPresenter() = ForecastPresenter(cityName)

    override fun initComponent(component: ActivityComponent) {
        component.inject(this)
    }

    // base

    override fun initOrRestoreArgs(bundle: Bundle?) {
        cityName = bundle?.getString(EXTRA_CITY, null)
    }

    @SuppressLint("RestrictedApi")
    override fun initViews() {
        setSupportActionBar(binding.toolbar)
        binding.toolbar.setNavigationOnClickListener { finish() }
        supportActionBar?.apply {
            title = cityName?.toCityNameResString(this@ForecastActivity)
            setDefaultDisplayHomeAsUpEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
        forecastAdapter = ForecastAdapter(cityName)
        binding.rvWeather.apply {
            layoutManager = LinearLayoutManager(this@ForecastActivity)
            itemAnimator = DefaultItemAnimator()
            addItemDecoration(MarginItemDecorator(8, resources))
            adapter = forecastAdapter
        }
        binding.swipeRefreshLayout.setOnRefreshListener {
            presenter.fetchForecastAction.onNext(Unit)
        }
    }

    // mvp

    override fun showLoading(show: Boolean) {
        binding.swipeRefreshLayout.isRefreshing = show
    }

    override fun showWeathers(weathers: RealmResults<DayWeather>) {
        forecastAdapter.updateData(weathers)
    }

}