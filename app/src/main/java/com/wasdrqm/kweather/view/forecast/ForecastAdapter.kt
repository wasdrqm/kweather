package com.wasdrqm.kweather.view.forecast

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.wasdrqm.kweather.R
import com.wasdrqm.kweather.data.models.db.DayWeather
import com.wasdrqm.kweather.databinding.ItemForecastBinding
import com.wasdrqm.kweather.utils.DateTimeUtils
import io.realm.RealmRecyclerViewAdapter

class ForecastAdapter(private val city: String?) :
    RealmRecyclerViewAdapter<DayWeather, ForecastAdapter.ForecastViewHolder>(null, false, false) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ForecastViewHolder(
        ItemForecastBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        getItem(position)?.let(holder::bindData)
    }

    inner class ForecastViewHolder(private val binding: ItemForecastBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindData(weather: DayWeather) {
            binding.tvWeatherDescription.text = weather.description
            binding.ivWeather.load(itemView.context.getString(R.string.format_icon, weather.icon))
            binding.tvDateTime.text = weather.datetime?.let { date ->
                DateTimeUtils.getFormatDateTime(date, city)
            }
            binding.tvTempDay.text = itemView.context.getString(
                R.string.format_celsius, weather.dayTemperature?.toInt()
            )
            binding.tvTempMorning.text = itemView.context.getString(
                R.string.format_celsius, weather.morningTemperature?.toInt()
            )
            binding.tvTempEvening.text = itemView.context.getString(
                R.string.format_celsius, weather.eveningTemperature?.toInt()
            )
            binding.tvTempNight.text = itemView.context.getString(
                R.string.format_celsius, weather.nightTemperature?.toInt()
            )
        }

    }

}