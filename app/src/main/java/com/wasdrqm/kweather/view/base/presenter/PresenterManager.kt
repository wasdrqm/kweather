package com.wasdrqm.kweather.view.base.presenter

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PresenterManager @Inject constructor() {

    private val presenterMap = HashMap<Int, BasePresenter<*>>()

    @Suppress("UNCHECKED_CAST")
    fun <P : BasePresenter<*>> getPresenter(hashCode: Int, createPresenter:() -> P): P {
        var presenter = presenterMap[hashCode]
        if (presenter == null) {
            presenter = createPresenter()
            presenterMap[hashCode] = presenter
        }
        return presenter as P
    }

    fun releasePresenter(hashCode: Int) = presenterMap.remove(hashCode)

    fun releaseAllPresenters() = presenterMap.clear()

}
