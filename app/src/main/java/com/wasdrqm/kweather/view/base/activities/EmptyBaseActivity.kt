package com.wasdrqm.kweather.view.base.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleRegistry
import com.evernote.android.state.StateSaver

abstract class EmptyBaseActivity : AppCompatActivity() {

    protected abstract val layoutId: Int?
    protected open lateinit var lifecycleRegistry: LifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutId?.let(this::setContentView)

        lifecycleRegistry = LifecycleRegistry(this)
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)

        StateSaver.restoreInstanceState(this, savedInstanceState)
        if (savedInstanceState == null) initArgs(intent.extras)
        else restoreBundlesData()

        initCore()
        initViews(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this, outState)
    }

    override fun onStart() {
        super.onStart()
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }

    override fun onResume() {
        super.onResume()
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        super.onPause()
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    }

    override fun onStop() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
        super.onStop()
    }

    override fun onDestroy() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        super.onDestroy()
    }

    /**
     * init presenters
     */
    protected open fun initCore() {}

    /**
     * init intent extra values
     */
    protected open fun initArgs(bundle: Bundle?) {}

    /**
     * restore state for bundles
     */
    protected open fun restoreBundlesData() {}

    /**
     * init layout views and actions
     */
    protected open fun initViews(savedInstanceState: Bundle?) {}

}