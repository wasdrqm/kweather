package com.wasdrqm.kweather.view.base.presenter

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.github.ajalt.timberkt.d
import com.wasdrqm.kweather.core.WeatherApplication
import com.wasdrqm.kweather.core.di.components.DaggerPresenterComponent
import com.wasdrqm.kweather.core.di.components.PresenterComponent
import com.wasdrqm.kweather.view.base.IMvpView
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm

abstract class BasePresenter<V: IMvpView> : LifecycleObserver {

    protected lateinit var view: V
    protected lateinit var realm: Realm

    private val createSubscriptions = CompositeDisposable()
    private val startSubscriptions = CompositeDisposable()
    private val resumeSubscriptions = CompositeDisposable()

    protected abstract fun initComponent(component: PresenterComponent)
    protected abstract fun onStartAction(subscriptions: CompositeDisposable)

    fun setPresenter(view: V, realm: Realm) {
        this.view = view
        this.realm = realm
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    protected fun onCreate() {
        initComponent(DaggerPresenterComponent.builder().appComponent(WeatherApplication.appComponent).build())
        if (!realm.isClosed) {
            onCreateAction(createSubscriptions)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    protected fun onStart() {
        if (!realm.isClosed) {
            onStartAction(startSubscriptions)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    protected fun onResume() {
        onResumeAction()
        if (!realm.isClosed) {
            onResumeAction(resumeSubscriptions)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    protected fun onPause() {
        onPauseAction()
        resumeSubscriptions.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    protected fun onStop() {
        onStopAction()
        startSubscriptions.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    protected fun onDestroy() {
        onDestroyAction()
        createSubscriptions.clear()
    }

    protected open fun onCreateAction(subscriptions: CompositeDisposable) {
        // do nothing
    }

    protected open fun onResumeAction(subscriptions: CompositeDisposable) {
        // do nothing
    }

    protected open fun onDestroyAction() {
        // do nothing
    }

    protected open fun onStopAction() {
        // do nothing
    }

    protected open fun onResumeAction() {
        // do nothing
    }

    protected open fun onPauseAction() {
        // do nothing
    }

}