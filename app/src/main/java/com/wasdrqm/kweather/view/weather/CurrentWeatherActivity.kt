package com.wasdrqm.kweather.view.weather

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import com.evernote.android.state.State
import com.wasdrqm.kweather.R
import com.wasdrqm.kweather.core.di.components.ActivityComponent
import com.wasdrqm.kweather.data.models.db.CurrentWeather
import com.wasdrqm.kweather.databinding.ActivityCurrentWeatherBinding
import com.wasdrqm.kweather.extensions.toCityNameResString
import com.wasdrqm.kweather.utils.DateTimeUtils
import com.wasdrqm.kweather.view.base.activities.BaseMvpActivity
import com.wasdrqm.kweather.view.forecast.ForecastActivity

class CurrentWeatherActivity : BaseMvpActivity<ActivityCurrentWeatherBinding, ICurrentWeatherView, CurrentWeatherPresenter>(),
    ICurrentWeatherView {

    companion object {

        private const val EXTRA_CITY = "extra::city_name"

        fun openScreen(context: Context, cityName: String?) {
            context.startActivity(
                Intent(context, CurrentWeatherActivity::class.java).putExtra(EXTRA_CITY, cityName)
            )
        }

    }

    @State var cityName: String? = null

    // base

    override val view = this
    override val bindingInflater: (LayoutInflater) -> ActivityCurrentWeatherBinding
        = ActivityCurrentWeatherBinding::inflate

    override fun getPresenterCode() = ("today_$cityName").hashCode()
    override fun createPresenter() = CurrentWeatherPresenter(cityName)

    override fun initComponent(component: ActivityComponent) {
        component.inject(this)
    }

    // views

    override fun initOrRestoreArgs(bundle: Bundle?) {
        cityName = bundle?.getString(EXTRA_CITY, null)
    }

    @SuppressLint("RestrictedApi")
    override fun initViews() {
        setSupportActionBar(binding.toolbar)
        binding.toolbar.setNavigationOnClickListener { finish() }
        supportActionBar?.apply {
            title = cityName?.toCityNameResString(this@CurrentWeatherActivity)
            setDefaultDisplayHomeAsUpEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
        binding.buttonForecast.setOnClickListener { ForecastActivity.openScreen(this, cityName) }
        binding.swipeRefreshLayout.setOnRefreshListener {
            presenter.fetchWeatherAction.onNext(Unit)
        }
    }

    // mvp methods

    override fun showProgress(show: Boolean) {
        binding.swipeRefreshLayout.isRefreshing = show
    }

    override fun showContent(weather: CurrentWeather) {
        weather.datetime?.let { date ->
            binding.tvLastUpdate.text = DateTimeUtils.getFormatDateTime(date, cityName)
        }
        binding.tvCurrentWeather.text = weather.description
        binding.tvTemp.text = getString(R.string.format_celsius, weather.temperature?.toInt())
        binding.tvMinTemp.text = getString(R.string.format_celsius, weather.minTemperature?.toInt())
        binding.tvMaxTemp.text = getString(R.string.format_celsius, weather.maxTemperature?.toInt())
        binding.tvHumidity.text = weather.humidity.toString()
        binding.tvPressure.text = weather.pressure.toString()
        weather.sunset?.let { date ->
            binding.tvSunset.text = DateTimeUtils.getFormatDateTime(date, cityName)
        }
        weather.sunrise?.let { date ->
            binding.tvSunrise.text = DateTimeUtils.getFormatDateTime(date, cityName)
        }
    }

}