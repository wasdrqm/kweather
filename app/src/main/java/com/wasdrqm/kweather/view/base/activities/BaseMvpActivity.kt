package com.wasdrqm.kweather.view.base.activities

import androidx.lifecycle.LifecycleObserver
import androidx.viewbinding.ViewBinding
import com.wasdrqm.kweather.view.base.IMvpView
import com.wasdrqm.kweather.view.base.presenter.BasePresenter
import com.wasdrqm.kweather.view.base.presenter.PresenterManager
import io.realm.Realm
import javax.inject.Inject

abstract class BaseMvpActivity<B: ViewBinding, V: IMvpView, P> : BaseActivity<B>()
    where P : BasePresenter<V>, P : LifecycleObserver {

    protected abstract val view: V
    protected abstract fun getPresenterCode(): Int
    protected abstract fun createPresenter(): P

    @Inject
    protected open lateinit var presenterManager: PresenterManager
    protected open lateinit var presenter: P
    protected open lateinit var realm: Realm

    override fun initCore() {
        realm = Realm.getDefaultInstance()
        presenter = presenterManager.getPresenter(getPresenterCode(), this::createPresenter)
        presenter.setPresenter(view, realm)
        lifecycle.addObserver(presenter)
    }

    override fun onDestroy() {
        if (isFinishing) {
            presenterManager.releasePresenter(getPresenterCode())
        }
        lifecycle.removeObserver(presenter)
        realm.close()
        super.onDestroy()
    }

}