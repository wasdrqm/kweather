package com.wasdrqm.kweather.view.weather

import com.github.ajalt.timberkt.d
import com.github.ajalt.timberkt.e
import com.wasdrqm.kweather.core.di.components.PresenterComponent
import com.wasdrqm.kweather.data.DataRepository
import com.wasdrqm.kweather.data.models.db.CurrentWeather
import com.wasdrqm.kweather.extensions.applyNetworkSchedulers
import com.wasdrqm.kweather.extensions.handleError
import com.wasdrqm.kweather.extensions.realmTransaction
import com.wasdrqm.kweather.extensions.retryWithDelay
import com.wasdrqm.kweather.view.base.IMvpView
import com.wasdrqm.kweather.view.base.presenter.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class CurrentWeatherPresenter(private val city: String?) : BasePresenter<ICurrentWeatherView>() {

    @Inject lateinit var dataRepository: DataRepository

    val fetchWeatherAction = PublishSubject.create<Unit>()

    override fun initComponent(component: PresenterComponent) {
        component.inject(this)
    }

    override fun onCreateAction(subscriptions: CompositeDisposable) {
        subscriptions.add(fetchWeatherAction
            .flatMapSingle { dataRepository
                .fetchCurrentWeather(city)
                .handleError { error -> e { "#weather-presenter fetch current weather error, message: $error" } }
                .retryWithDelay(3000L, 3)
                .applyNetworkSchedulers()
            }
            .realmTransaction(realm, dataRepository::saveCurrentWeather)
            .subscribe(
                { d { "#weather-presenter fetch current weather success" } },
                { throwable -> e(throwable) { "#weather-presenter fetch current weather error" } }
            )
        )
        fetchWeatherAction.onNext(Unit)
    }

    override fun onStartAction(subscriptions: CompositeDisposable) {
        subscriptions.add(dataRepository
            .getCurrentWeather(realm, city)
            .distinctUntilChanged()
            .doOnSubscribe { view.showProgress(true) }
            .doOnNext { view.showProgress(false) }
            .subscribe(view::showContent)
        )
    }

}

interface ICurrentWeatherView : IMvpView {
    fun showProgress(show: Boolean)
    fun showContent(weather: CurrentWeather)
}