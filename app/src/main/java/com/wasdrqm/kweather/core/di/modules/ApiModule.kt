package com.wasdrqm.kweather.core.di.modules

import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.wasdrqm.kweather.BuildConfig
import com.wasdrqm.kweather.data.network.RxErrorHandlingCallAdapterFactory
import com.wasdrqm.kweather.data.network.WeatherApiRequests
import dagger.Module
import dagger.Provides
import io.realm.RealmObject
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule {

    companion object {
        private const val BASE_URL = "http://api.openweathermap.org"
    }

    @Named("ApiConverterFactory")
    @Provides
    @Singleton
    fun provideConverter(): Converter.Factory {
        val builder = GsonBuilder()
        builder.setLenient()
        builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        builder.setExclusionStrategies(object : ExclusionStrategy {
            override fun shouldSkipField(f: FieldAttributes): Boolean {
                return f.declaringClass == RealmObject::class.java
            }

            override fun shouldSkipClass(clazz: Class<*>): Boolean {
                return false
            }
        })
        return GsonConverterFactory.create(builder.create())
    }

    @Named("ApiClient")
    @Provides
    @Singleton
    fun provideClient() : OkHttpClient {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
            builder.addInterceptor(loggingInterceptor)
        }
        builder.connectTimeout(60, TimeUnit.SECONDS)
        builder.readTimeout(60, TimeUnit.SECONDS)
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideWeatherApi(
        @Named("ApiClient") client: OkHttpClient,
        @Named("ApiConverterFactory") converter: Converter.Factory
    ): WeatherApiRequests {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(converter)
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
            .build()
        return retrofit.create(WeatherApiRequests::class.java)
    }

}