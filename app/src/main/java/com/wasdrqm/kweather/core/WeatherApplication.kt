package com.wasdrqm.kweather.core

import android.app.Application
import com.wasdrqm.kweather.core.di.components.AppComponent
import com.wasdrqm.kweather.core.di.components.DaggerAppComponent
import com.wasdrqm.kweather.core.di.modules.AppModule
import io.realm.BuildConfig
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.log.LogLevel
import io.realm.log.RealmLog
import timber.log.Timber

class WeatherApplication : Application() {

    companion object {
        lateinit var appComponent: AppComponent
        const val MOSCOW = "Moscow"
        const val NOVOSIBIRSK = "Novosibirsk"
        const val TASHKENT = "Tashkent"
    }

    override fun onCreate() {
        super.onCreate()
        // DI Dagger 2
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        appComponent.inject(this)
        // Realm
        Realm.init(this)
        Realm.setDefaultConfiguration(
            RealmConfiguration.Builder()
                .allowWritesOnUiThread(true)
                .deleteRealmIfMigrationNeeded()
                .build()
        )
        if (BuildConfig.DEBUG) {
            RealmLog.setLevel(LogLevel.ERROR)
        }
        Timber.plant(Timber.DebugTree())
    }

}