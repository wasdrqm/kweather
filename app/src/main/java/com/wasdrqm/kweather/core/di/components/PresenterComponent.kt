package com.wasdrqm.kweather.core.di.components

import com.wasdrqm.kweather.core.di.scopes.PerPresenter
import com.wasdrqm.kweather.view.forecast.ForecastPresenter
import com.wasdrqm.kweather.view.weather.CurrentWeatherPresenter
import dagger.Component

@PerPresenter
@Component(dependencies = [(AppComponent::class)])
interface PresenterComponent {
    // presenters
    fun inject(presenter: CurrentWeatherPresenter)
    fun inject(forecastPresenter: ForecastPresenter)
}