package com.wasdrqm.kweather.core.di.modules

import android.content.Context
import com.wasdrqm.kweather.core.WeatherApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private val application: WeatherApplication) {

    @Provides
    @Singleton
    fun provideApp(): WeatherApplication = application

    @Provides
    @Singleton
    fun provideContext(): Context = application.applicationContext

}