package com.wasdrqm.kweather.core.di.components

import com.wasdrqm.kweather.core.di.scopes.PerActivity
import com.wasdrqm.kweather.view.forecast.ForecastActivity
import com.wasdrqm.kweather.view.weather.CurrentWeatherActivity
import dagger.Component

@PerActivity
@Component(dependencies = [(AppComponent::class)])
interface ActivityComponent {
    fun inject(activity: ForecastActivity)
    fun inject(activity: CurrentWeatherActivity)
}