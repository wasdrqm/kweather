package com.wasdrqm.kweather.core.di.components

import android.content.Context
import com.wasdrqm.kweather.core.WeatherApplication
import com.wasdrqm.kweather.core.di.modules.ApiModule
import com.wasdrqm.kweather.core.di.modules.AppModule
import com.wasdrqm.kweather.data.DataRepository
import com.wasdrqm.kweather.view.base.presenter.PresenterManager
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (ApiModule::class)])
interface AppComponent {
    fun app(): WeatherApplication
    fun appContent(): Context

    fun appPresenterManager(): PresenterManager
    fun appDataRepository(): DataRepository

    fun inject(application: WeatherApplication)
}