package com.wasdrqm.kweather.core.di.components

import com.wasdrqm.kweather.core.di.scopes.PerPresenter
import dagger.Component

@PerPresenter
@Component(dependencies = [(AppComponent::class)])
interface FragmentComponent